
# coding: utf-8

# In[3]:


from bs4 import BeautifulSoup
import urllib.request
import re

## dictionary for saving history records
record = {}
url = "https://www.52kjwang.com/pk10/kaijiang?count=100"
content = urllib.request.urlopen(url).read()
soup = BeautifulSoup(content, "html.parser")

for tag in soup.find_all("div", "num_pk10"):
  idx = tag.parent.find_previous_sibling('td').string.split()
  record[int(idx[0])] = [] 
  #print(idx[0])
  #tag.find_all("span")
  for j,i in enumerate(tag.find_all("span")):
    num = re.findall(r'\d+', i["class"][0])[0]    
    record[int(idx[0])].insert(j, int(num)) 
    #print(num, end=' ')    
  #print("\n##")

#print('開獎紀錄(共%d筆)：' %(len(record)))
#record

## read in history records, update new records
import gspread
from oauth2client.service_account import ServiceAccountCredentials

scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
credentials = ServiceAccountCredentials.from_json_keyfile_name('My Project 57522-6c0a1bc7cc42.json', scope)
gc = gspread.authorize(credentials)

# Open our new sheet and add some data.
worksheet = gc.open_by_key('16EWCoQ-KK7wpvOcu2RgxCCCLmWn1c9Azmb359-RuIp4').get_worksheet(0)

#check 1st row
if worksheet.acell('A1').value is "":
  worksheet.delete_row(1)
  
#check for redundant
aKeys = list(map(int, record.keys()))
aKeys.reverse()
count = 0
flag = False
for i in aKeys:
  if flag is False:
    val = worksheet.acell('A1').value #儘量減少與google sheet互動次數，一天只能100次
    if val is "" or i > int(val):
      flag = True
      worksheet.insert_row([i]+record[i])  #insert new row
      count += 1
  else:
    worksheet.insert_row([i]+record[i]) #insert new row
    count += 1

print('Update', count, 'rows. 最新一期:', aKeys[-1])

