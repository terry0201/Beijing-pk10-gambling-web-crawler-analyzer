iCarStart      = [1,2,3,4,5,6,7,8,9,10] #下第幾號車(此數字往前五台)
iTargetBenefit = 100     #目標獲利
iNumStart      = 693297 #起始期數
iNumTurns      = 10     #共下幾期
aConsecutiveCost = [1, 2, 5, 10, 20, 50, 100, 200, 500, 0, 0, 0, 0, 0, 0] #連幾金額
iProperty = 0 #起始資產
Gain = 4.7 #中獎賠率
Loss = 5.0 #沒中賠率
aConsecutive   = []      #連幾
iRank          = []    #起始下第幾名

#array for car betting
aCar = [i+1 for i in range(10)]
aCar.extend(aCar)

import sys
import gspread
from oauth2client.service_account import ServiceAccountCredentials

# Open sheet and read data.
scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
credentials = ServiceAccountCredentials.from_json_keyfile_name('My Project 57522-6c0a1bc7cc42.json', scope)
gc = gspread.authorize(credentials)
worksheet = gc.open_by_key('16EWCoQ-KK7wpvOcu2RgxCCCLmWn1c9Azmb359-RuIp4').get_worksheet(0)
iStartRow = worksheet.find(str(iNumStart)).row
prev_record = { i : worksheet.row_values(iStartRow+i) for i in range(16) }
  
# 從過去推算起始連幾與下注名次  
for car_idx, car in enumerate(iCarStart):

  iRank.append(prev_record[0].index(str(car)))
  Consecutive = 0
  
  try:
    for i in range(15): #最多追溯前15
      car_rank_prev = prev_record[i+1].index(str(car))
      for car_tmp in range(car+9, car+9-5, -1): #check consecutive 5 cars
        if aCar[car_tmp] == int(prev_record[i][car_rank_prev]): #match
          aConsecutive.append(Consecutive)
          raise StopIteration 
      Consecutive += 1
  except StopIteration:
    pass

print('下注車號 =', iCarStart)    
print('初始排名 =', iRank)
print('初始連幾 =', aConsecutive)


# 開始下注
try:
  for turn in range(iNumTurns):
    curr_record = worksheet.row_values(iStartRow-turn-1)
    print(int(curr_record[0]), '期開獎', curr_record[1:len(curr_record)])

    for idx in range(0, len(iCarStart)):
      print('  下注%2d車第%2d名連%2d金額%4d' %(iCarStart[idx], iRank[idx], aConsecutive[idx], aConsecutiveCost[aConsecutive[idx]]), end=' ')
      bWin = False
      carWin = 0
      for car in range(iCarStart[idx]+9, iCarStart[idx]+9-5, -1): #check consecutive 5 cars
        if aCar[car] == int(curr_record[iRank[idx]]): #win
          bWin = True
          carWin = aCar[car]
          break

      if bWin is True:
        # win -> compute gain
        print('O中獎:%2d車' %(carWin), end=' ')
        if aConsecutive[idx] <= 15:
          iProperty += Gain*aConsecutiveCost[aConsecutive[idx]]
          #if iProperty >= iTargetBenefit:
          #  raise StopIteration
        aConsecutive[idx] = 0
        print('總資產=%7.2f' %(iProperty))
      else:
        # lose -> change betting to this rank
        print('X沒中     ', end=' ')
        if aConsecutive[idx] <= 15:
          for rank in range(10):
            if int(curr_record[rank+1]) == iCarStart[idx]:
              iRank[idx] = rank+1
              break
          iProperty -= Loss*aConsecutiveCost[aConsecutive[idx]]
          print('總資產=%7.2f' %(iProperty))
        aConsecutive[idx] += 1
    if iProperty >= iTargetBenefit:
      raise StopIteration
       
except StopIteration:
  print('達標！')
  pass
        
print('Final總資產 = $%7.2f' %(iProperty))