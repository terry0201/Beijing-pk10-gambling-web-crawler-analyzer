# Beijing-pk10-gambling-web-crawler-analyzer

這個專案包含了針對[北京賽車](https://www.52kjwang.com/pk10/)設計的網路爬蟲以及下注分析程式。

This is a web crawler and analyzer for Beijing-pk10 gambling (https://www.52kjwang.com/pk10/).

Two programs are included in this project and they are written with Python3:
* pk10_parse.py -- web crawler using BeautifulSoup4 python package and auto save to a Google spreadsheet
* pk10.py -- gambling simulator based on the web crawled record
  
I also make this project runnable online at [Google CoLab](https://colab.research.google.com/drive/1ZYSqbOtHz7v0I-9YjSlezY-xZBNRQtkL)

## Prerequisites
* Python3

## Built With  
* [OAuth2](http://gspread.readthedocs.io/en/latest/oauth2.html) - for Google spreadsheet authorization
* [gspread](http://gspread.readthedocs.io/en/latest/#gspread.models.Worksheet.insert_row) - to read/write Google spreadsheet
* [BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/) - web clawler package for python

## Reference
* [Access google sheets in python using Gspread](http://www.indjango.com/access-google-sheets-in-python-using-gspread/) - other Google spreadsheet authorization methods
* [Google APIs Dashboard](https://console.developers.google.com/) - monitoring the gspread usage (also apply higher quotas here)
* [w3schools.com Python Tutorial](https://www.w3schools.com/python/default.asp) - good Python Tutorial

## Authors
* **Terry, Chia-Hsin Chan 詹家欣** - *Initial work* - [terry0201/GitLab](https://gitlab.com/terry0201)